﻿using System;
using System.Diagnostics;

namespace ConsoleApp2
{
    internal class TimingAlg
    {
        TimeSpan duration; //хранение результата измерения
      //  TimeSpan[] threads; // значения времени для всех потоков процесса

        private TimeSpan startingTime;
        public TimingAlg()
        {
            //  duration = new TimeSpan(0);
            //   threads = new TimeSpan[Process.GetCurrentProcess().Threads.Count];
            startingTime = new TimeSpan(0);
            duration = new TimeSpan(0);

        }
        internal void StartTime()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            //   for (int i = 0; i < threads.Length; i++)
            //  threads[i] = Process.GetCurrentProcess().Threads[i].UserProcessorTime;
            startingTime = Process.GetCurrentProcess().Threads[0].UserProcessorTime;
        }

        internal void StopTime()
        {
            //    TimeSpan tmp;
            //   for (int i = 0; i < threads.Length; i++)
            //   {
            //        tmp = Process.GetCurrentProcess().Threads[i].
            //       UserProcessorTime.Subtract(threads[i]);
            //        if (tmp > TimeSpan.Zero)
            //            duration = tmp;
            //    }
            duration = Process.GetCurrentProcess().Threads[0]
                .UserProcessorTime.Subtract(startingTime);
        }

        internal object Result()
        {
            return duration;
        }
    }
}